from tornado.util import ObjectDict


def doc2obj(docs):
    if isinstance(docs, dict):
        for k, v in docs.items():
            docs[k] = doc2obj(v)
        return ObjectDict(**docs)
    elif isinstance(docs, list):
        for i in range(len(docs)):
            docs[i] = doc2obj(docs[i])
        return docs 
    else:
        return docs
    # return [ObjectDict(**x) for x in docs]