import os

from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop
from tornado import log

from .routes import urls


application = Application(urls, debug=True,
autoreload=False,
static_path=os.path.join(os.path.dirname(__file__), 'static'),
template_path=os.path.join(os.path.dirname(__file__), 'pages'),
cookie_secret='#fjljf@#fj?Fsd&'
)


def run_app():
    log.enable_pretty_logging()
    application.listen(8000)
    IOLoop.current().start()
