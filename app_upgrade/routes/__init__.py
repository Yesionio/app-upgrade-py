from typing import Any
from tornado.util import ObjectDict
from tornado.web import RequestHandler
from tinydb import TinyDB, Query


class BaseHandler(RequestHandler):
    def initialize(self):
        self._tiny_db = TinyDB('xon.json')
        _projects = self._tiny_db.table('projects')
        _versions = self._tiny_db.table('versions')
        _sessions = self._tiny_db.table('sessions')
        _users = self._tiny_db.table('users')
        self._db = ObjectDict()
        self._db.projects = _projects
        self._db.sessions = _sessions
        self._db.users = _users
        self._db.versions = _versions

    @property
    def db(self):
        return self._db

    def get_login_url(self) -> str:
        return '/login'

    def get_current_user(self) -> Any:
        if hasattr(self, '_user'):
            return self._user
        else:
            return None
    
    def on_finish(self):
        pass

class AuthHandler(BaseHandler):
    def prepare(self):
        sid = self.get_secure_cookie('sid')
        if sid is None:
            self.redirect(self.get_login_url())
        else:
            user = self.db.sessions.search(Query().sid == sid.decode())
            if user:
                username = user[0]['username']
                self._user = username
            else:
                self.redirect(self.get_login_url())


from . import home, auth, api

urls = [
    (r'/login', auth.LoginHandler),
    (r'/logout', auth.LogoutHandler),
    (r'/', home.IndexHandler),
    (r'/addProject', home.AddProjectHandler),
    (r'/versionList/(\S+)', home.VersionListHandler),
    (r'/addVersion/(\S+)', home.AddVersionHandler),
    (r'/download/(\S+)/(\S+)', api.DownloadFileHandler),
    (r'/api/checkUpdate/(\S+)', api.CheckLatestVersionHandler),
]
