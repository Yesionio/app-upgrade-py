import uuid

from tinydb.queries import Query
from . import BaseHandler

class LoginHandler(BaseHandler):
    def get(self):
        self.render('login.html')
    
    def post(self):
        userName = self.get_body_argument('userName', None)
        if userName is None:
            self.redirect('/login')
        else:
            a = self.db.users.contains(Query().username==userName)
            if a:
                sid = str(uuid.uuid4())
                self.db.sessions.insert({'sid': sid, 'username': userName})
                self.set_secure_cookie('sid', sid, 30)
                self.redirect('/')
            else:
                print('fail')
                self.redirect('/login')


class LogoutHandler(AuthHandler):
    def get(self):
        sid = self.get_secure_cookie('sid')
        self.db.sessions.remove(Query().sid == sid.decode())
        self.clear_all_cookies()
        self.redirect('/')