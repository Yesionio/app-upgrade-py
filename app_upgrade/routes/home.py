from datetime import datetime
import os
import time
from uuid import uuid4
from tornado.util import ObjectDict
from app_upgrade.utils import doc2obj
from tinydb.queries import Query
from app_upgrade.routes import AuthHandler


class IndexHandler(AuthHandler):
    def get(self):
        # Project = Query()
        l = self.db.projects.search(Query().username==self.get_current_user())
        project_list = reversed(doc2obj(l))
        self.render('index.html', projects=project_list, username=self.get_current_user())


class AddProjectHandler(AuthHandler):
    def get(self):
        self.render('addProject.html')

    def post(self):
        projectName = self.get_body_argument('projectName')
        description = self.get_body_argument('description')
        if self.db.projects.contains(Query().projectName == projectName):
            self.render('addProject.html', err_msg='Project name is exists')
            return
        
        proj = ObjectDict()
        t = datetime.now()
        proj.projectName = projectName
        proj.description = description
        proj.createDate = t.strftime('%Y-%m-%d %H:%M:%S')
        proj.appCode = int(round(t.timestamp() * 1000))
        proj.username = self.get_current_user()

        self.db.projects.insert(proj)

        self.redirect('/')


class VersionListHandler(AuthHandler):
    def get(self, projectName):
        versionList = self.db.versions.search(Query().projectName == projectName)
        versionList = doc2obj(versionList)
        for x in versionList:
            x.description = x.description.replace('\r', '')
            x.description = x.description.replace('\n', '<br>')
        versionList.reverse()
        
        self.render('versions.html',
        projectName=projectName,
        versionList = versionList)


class AddVersionHandler(AuthHandler):
    def get(self, projectName):
        self.render('addVersion.html', projectName=projectName)

    def post(self, projectName):
        version = self.get_body_argument('version')
        description = self.get_body_argument('description')
        file = self.request.files.get('file')[0]
        forceUpdate = bool(self.get_body_argument('forceUpdate', False))

        if self.db.versions.contains((Query().version == version) & (Query().projectName == projectName)):
            self.render('addVersion.html', err_msg='The version is exists')
            return
        
        ver = ObjectDict()
        t = datetime.now()
        f = str(uuid4())
        save_path = os.path.join(os.curdir, 'uploads')
        ver.projectName = projectName
        ver.version = version
        ver.description = description
        ver.fileName = file['filename']
        ver.realFileName = f
        ver.forceUpdate = forceUpdate
        ver.createDate = int(round(t.timestamp() * 1000))

        with open(os.path.join(save_path, f), 'wb') as save_file:
            save_file.write(file['body'])
        
        self.db.versions.insert(ver)
        self.db.projects.update({'latestVersion': version}, Query().projectName == projectName)

        self.redirect('/versionList/'+projectName)
