import os, json
from tinydb import Query
from tornado.escape import url_escape

from app_upgrade.routes import BaseHandler
from app_upgrade.utils import doc2obj


class DownloadFileHandler(BaseHandler):
    def get(self, projectName, version):
        Version = Query()
        rls = self.db.versions.search((Version.projectName == projectName) & (Version.version == version))
        if not rls:
            self.send_error(404, reason='File not found!')
            return
        ver = doc2obj(rls[0])
        save_path = os.path.join(os.curdir, 'uploads')

        with open(os.path.join(save_path, ver.realFileName), 'rb') as f:
            while True:
                data = f.read(1024)
                if not data:
                    break
                self.write(data)

        self.set_header('Content-Type', 'application/octet-stream')
        self.set_header('Content-Disposition', 'attachment; filename={}'.format(url_escape(ver.fileName)))
        self.finish()


class CheckLatestVersionHandler(BaseHandler):
    def get(self, projectName):
        rls = self.db.projects.search(Query().projectName == projectName)
        if not rls:
            self.send_error(404, reason='File not found!')
            return
        latestVersion = rls[0].get('latestVersion', None)
        if latestVersion is None:
            self.send_error(404, reason='File not found!')
            return
        qVersion = Query()
        version = self.db.versions.search((qVersion.projectName == projectName) & (qVersion.version == latestVersion))[0]
        data = json.dumps(version)

        self.set_header('Content-Type', 'application/json')
        self.write(data)
        self.finish()
